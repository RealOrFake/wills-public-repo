
import Node
from Node import Node

def buildNodeFromList(list):
    if len(list) == 1:
        return Node(list[0], None)
    else:
        head, *tail = list
        return Node(head, buildNodeFromList(tail))

def printEntireNode(node):
    if node is None:
        return
    print(node.data)
    printEntireNode(node.next)
    
def areNodesEqual(head1, head2):
    if head1 is None and head2 is None:
        return True
    elif head1 is None and head2 is not None:
        return False
    elif head1 is not None and head2 is None:
        return False
    else:
        data1 = head1.data
        data2 = head2.data
        next1 = head1.next
        next2 = head2.next
        return (data1 == data2) and areNodesEqual(next1, next2)