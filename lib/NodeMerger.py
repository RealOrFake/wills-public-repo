

import Node
from Node import Node


def merge(head1, head2):
    if head1 is None and head2 is None: # test_merge_null_nodes
        return None
    elif head1 is not None and head2 is None: # test_merge_null_node1
        return head1
    elif head1 is None and head2 is not None: #test_merge_null_node2
        return head2
    else:
        result_head = None
        pointer = None
        tmp1 = head1
        tmp2 = head2
        
        tmp_data1 = tmp1.data
        tmp_data2 = tmp2.data
        
        if tmp_data1 <= tmp_data2: 
            result_head = tmp1
            pointer = tmp1
        else:
            result_head = tmp2
            pointer = tmp2
        
        while tmp1 is not None and tmp2 is not None:
            tmp_data1 = tmp1.data
            tmp_data2 = tmp2.data
            tmp1_next = tmp1.next
            tmp2_next = tmp2.next
            if tmp_data1 < tmp_data2:
                if tmp1_next is None or tmp1_next.data > tmp_data2: # test_1_22 & test_13_22
                    tmp1.next = tmp2
                    pointer = tmp2
                else: # test_12_22
                    pointer = tmp1_next
                tmp1 = tmp1_next
            elif tmp_data1 > tmp_data2: 
                if tmp2_next is None or tmp2_next.data > tmp_data1: #test_22_1 & test_22_13
                    tmp2.next = tmp1
                    pointer = tmp1
                else: # test_22_12
                    pointer = tmp2_next
                tmp2 = tmp2_next
            elif tmp_data1 == tmp_data2:
                if tmp1_next is None:
                    if pointer == tmp1: # test_1_12
                        tmp1.next = tmp2
                    else: # test_12_22
                        tmp2.next = tmp1
                        tmp1.next = tmp2_next
                    tmp1 = tmp1_next
                elif tmp2_next is None:
                    if pointer == tmp2: # test_12_2
                        tmp2.next = tmp1
                    else:# test_22_2
                        tmp1.next = tmp2 
                        tmp2.next = tmp1_next
                    tmp2 = tmp2_next
                else:
                    if tmp1_next.data > tmp_data2:
                        if pointer == tmp1: # test_12_11
                            tmp1.next = tmp2
                            pointer = tmp2
                            tmp1 = tmp1_next
                        else: # test_123_22
                            tmp2.next = tmp1
                            pointer = tmp2_next
                            tmp2 = tmp2_next
                    elif tmp2_next.data > tmp_data1:
                        if pointer == tmp1: #test_11_12
                            tmp1.next = tmp2
                            tmp2.next = tmp1_next
                            pointer = tmp1_next
                            tmp1 = tmp1_next
                        else:# test_122_23
                            tmp2.next = tmp1
                            pointer = tmp1
                        tmp2 = tmp2_next        
                    else:
                        if pointer == tmp1: #test_11_11
                            pointer = tmp1_next
                            tmp1 = tmp1_next
                        else: #test_122_22
                            pointer = tmp2_next
                            tmp2 = tmp2_next
                        
            
       
        return result_head
            
        