from setuptools import setup
setup(
    name = "will'sRepo",
    version = "1.0.0",
    packages=['lib'],
    install_requires=['nose>=1.3'],
    setup_requires=['nose>=1.3'],
    zip_safe=False,
)