import unittest
import Node
from Node import Node
import NodeBuilder
import NodeMerger
class TestNodeMerger(unittest.TestCase):
    
    def test_merge_null_nodes(self):
        result = NodeMerger.merge(None, None)
        self.assertEqual(NodeBuilder.areNodesEqual(result, None), True)
        
    def test_merge_null_node1(self):
        node = NodeBuilder.buildNodeFromList([1])
        result = NodeMerger.merge(node, None)
        self.assertEqual(NodeBuilder.areNodesEqual(result, node), True)
    
    def test_merge_null_node2(self):
        node = NodeBuilder.buildNodeFromList([1])
        result = NodeMerger.merge(None, node)
        self.assertEqual(NodeBuilder.areNodesEqual(result, node), True)
        
    def test_merge_single_nodes(self):
        node1 = NodeBuilder.buildNodeFromList([1])
        node2 = NodeBuilder.buildNodeFromList([2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
        

     
    def test_11_11(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1])
        node2 = NodeBuilder.buildNodeFromList([1, 1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1, 1])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
        
    def test_1_11(self):
        node1 = NodeBuilder.buildNodeFromList([1])
        node2 = NodeBuilder.buildNodeFromList([1, 1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)    
     
    def test_11_1(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1])
        node2 = NodeBuilder.buildNodeFromList([1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
     
    def test_11_111(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1])
        node2 = NodeBuilder.buildNodeFromList([1, 1, 1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1, 1, 1])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)  
     
    def test_12_12(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2])
        node2 = NodeBuilder.buildNodeFromList([1, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
        
    def test_1_22(self):
        node1 = NodeBuilder.buildNodeFromList([1])
        node2 = NodeBuilder.buildNodeFromList([2, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)    
     
    def test_12_1(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2])
        node2 = NodeBuilder.buildNodeFromList([1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)  
    
    def test_11_12(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1])
        node2 = NodeBuilder.buildNodeFromList([1, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
     
    def test_122_23(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2, 2])
        node2 = NodeBuilder.buildNodeFromList([2, 3])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 2, 3])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
     
    def test_122_22(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2, 2])
        node2 = NodeBuilder.buildNodeFromList([2, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
    
    def test_123_22(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2, 3])
        node2 = NodeBuilder.buildNodeFromList([2, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 2, 3])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
        
    def test_12_11(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2])
        node2 = NodeBuilder.buildNodeFromList([1, 1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True) 
    
    def test_1_12(self):
        node1 = NodeBuilder.buildNodeFromList([1])
        node2 = NodeBuilder.buildNodeFromList([1, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
    def test_12_22(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2])
        node2 = NodeBuilder.buildNodeFromList([2, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
        
    def test_12_2(self):
        node1 = NodeBuilder.buildNodeFromList([1, 2])
        node2 = NodeBuilder.buildNodeFromList([2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
    
    def test_22_2(self):
        node1 = NodeBuilder.buildNodeFromList([2, 2])
        node2 = NodeBuilder.buildNodeFromList([2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([2, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
    def test_13_22(self):
        node1 = NodeBuilder.buildNodeFromList([1, 3])
        node2 = NodeBuilder.buildNodeFromList([2, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 3])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
        
    def test_22_1(self):
        node1 = NodeBuilder.buildNodeFromList([2, 2])
        node2 = NodeBuilder.buildNodeFromList([1])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
    def test_22_13(self):
        node1 = NodeBuilder.buildNodeFromList([2, 2])
        node2 = NodeBuilder.buildNodeFromList([1, 3])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 3])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
        
    def test_22_12(self):
        node1 = NodeBuilder.buildNodeFromList([2, 2])
        node2 = NodeBuilder.buildNodeFromList([1, 2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 2, 2, 2])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)
    
    def test_merge_same_nodes(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1, 2, 2, 3])
        node2 = NodeBuilder.buildNodeFromList([1, 1, 2, 2, 3])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 1, 1, 2, 2, 2, 2, 3, 3])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)  
        
    def test_merge_nodes_diff_len(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1, 4, 5])
        node2 = NodeBuilder.buildNodeFromList([2, 3, 9])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 2, 3, 4, 5, 9])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)  
    
    def test_merge1(self):
        node1 = NodeBuilder.buildNodeFromList([1, 1, 4, 5])
        node2 = NodeBuilder.buildNodeFromList([2])
        
        result = NodeMerger.merge(node1, node2)
        
        expected = NodeBuilder.buildNodeFromList([1, 1, 2, 4, 5])
        self.assertEqual(NodeBuilder.areNodesEqual(result, expected), True)  
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'TestNodeMerger.testNodeMergerInfo']
    unittest.main()