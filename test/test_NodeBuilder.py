
import unittest
import Node
from Node import Node
import NodeBuilder

class TestNodeBuilder(unittest.TestCase):
        
    def test_null_nodes_are_equal(self):
        self.assertEqual(NodeBuilder.areNodesEqual(None, None), True)
        
    def test_null_node_is_not_equal(self):
        node = Node(5, None)
        self.assertEqual(NodeBuilder.areNodesEqual(node, None), False)
        
    def test_single_nodes_are_equal(self):
        node = Node(5, None)
        self.assertEqual(NodeBuilder.areNodesEqual(node, node), True)   
     
    def test_single_nodes_are_not_equal(self):
        node1 = Node(5, None)
        node2 = Node(2, None)
        self.assertEqual(NodeBuilder.areNodesEqual(node1, node2), False)   
     
    def test_nodes_equal(self):
        node1 = NodeBuilder.buildNodeFromList([1,2,3])
        self.assertEqual(NodeBuilder.areNodesEqual(node1, node1), True)  
        
    def test_nodes_not_equal(self):
        node1 = NodeBuilder.buildNodeFromList([1,2,3])
        node2 = NodeBuilder.buildNodeFromList([1,2,3,4])
        self.assertEqual(NodeBuilder.areNodesEqual(node1, node2), False)  
        
    def test_nodes_not_equal2(self):
        node1 = NodeBuilder.buildNodeFromList([1,2,3])
        node2 = NodeBuilder.buildNodeFromList([1,4,3])
        self.assertEqual(NodeBuilder.areNodesEqual(node1, node2), False)     
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()